package com.cnblogs.reminis;

/**
 * @author Mr.Sun
 * @date 2022年09月09日 16:39
 *
 * 内置锁是可重入锁
 */
public class Widget {
    public synchronized void doSomething() {
    }
}

class LoggingWidget extends Widget {
    public synchronized void doSomething() {
        System.out.println(toString() + ": calling doSomething");
        super.doSomething();
    }
}
