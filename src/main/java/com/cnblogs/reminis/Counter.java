package com.cnblogs.reminis;

import net.jcip.annotations.ThreadSafe;

/**
 * @author Mr.Sun
 * @date 2022年09月10日 16:25
 *
 * 使用Java监视器模式的线程安全计数器
 */
@ThreadSafe
public final class Counter {
    private long value = 0;

    public synchronized long getValue() {
        return value;
    }

    public synchronized long increment() {
        if (value == Long.MAX_VALUE)
            throw new IllegalStateException("counter overflow");
        return ++value;
    }
}
