package com.cnblogs.reminis;

/**
 * @author Mr.Sun
 * @date 2022年09月10日 15:35
 *
 * 由于未被正确发布，因此这个类可能会出翔故障
 */
public class Holder {
    private int n;

    public Holder(int n) {
        this.n = n;
    }

    public void assertSanity() {
        if (n != n)
            throw new AssertionError("This statement is false.");
    }
}
