package com.cnblogs.reminis;

import com.cnblogs.annotation.NotThreadSafe;

/**
 * @author Mr.Sun
 * @date 2022年09月09日 16:02
 *
 * 非线程安全的延迟初始化
 */
@NotThreadSafe
public class LazyInitRace {
    private ExpensiveObject instance = null;

    public ExpensiveObject getInstance() {
        if (instance == null)
            instance = new ExpensiveObject();
        return instance;
    }
}

class ExpensiveObject { }
