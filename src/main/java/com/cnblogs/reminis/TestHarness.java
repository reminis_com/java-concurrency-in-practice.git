package com.cnblogs.reminis;

import java.util.concurrent.CountDownLatch;

/**
 * @author: Mr.Sun
 * @create: 2022-11-13 22:04
 * @description: 测试n个线程并发执行某个任务所需要的时间
 **/
public class TestHarness {

    public static void main(String[] args) throws InterruptedException {
        long time = timeTasks(5, () -> System.out.println("Mr.Sun"));
        System.out.println(time);
    }

    public static long timeTasks(int nThreads, final Runnable task) throws InterruptedException {
        // 起始门，初始值为1
        final CountDownLatch startGate = new CountDownLatch(1);
        // 结束门，初始值为工作线程数
        final CountDownLatch endGate = new CountDownLatch(nThreads);

        for (int i = 0; i < nThreads; i++) {
            Thread t = new Thread(() -> {
                try {
                    // 每个工作线程都会在启动门上等待，从而确保所有线程都就绪后才开始执行
                    startGate.await();
                    try {
                        task.run();
                    } finally {
                        // 每个线程要做的最后一件事就是调用结束门的countDown()方法减1
                        endGate.countDown();
                    }
                } catch (InterruptedException e) {
                }
            });
            t.start();
        }

        long start = System.nanoTime();
        // 起始门：使得主线程能够同时释放所有工作线程
        startGate.countDown();
        // 结束门：使主线程能够等待最后一个工作线程执行完成
        endGate.await();
        // 统计消耗时间
        long end = System.nanoTime();
        return end - start;
    }
}
