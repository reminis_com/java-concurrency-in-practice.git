package com.cnblogs.reminis;

/**
 * @author Mr.Sun
 * @date 2022年09月10日 15:35
 *
 * 不安全的发布对象
 */
public class StuffIntoPublic {
    public Holder holder;

    public void initialize() {
        holder = new Holder(42);
    }
}
