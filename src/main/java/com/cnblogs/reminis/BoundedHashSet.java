﻿package com.cnblogs.reminis;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.Semaphore;

/**
 * @author: Mr.Sun
 * @create: 2022-11-14 10:22
 * @description: 使用Semaphore为容器设置边界
 **/
public class BoundedHashSet<T> {
    private final Set<T> set;
    private final Semaphore semaphore;

    public BoundedHashSet(int bound) {
        this.set = Collections.synchronizedSet(new HashSet<>());
        // 初始化信号量的计数值为容器容量的最大值
        this.semaphore = new Semaphore(bound);
    }

    /**
     * 向容器中添加一个元素
     *
     * @param obj 代添加对象
     * @return 是否添加成功
     */
    public boolean add(T obj) throws InterruptedException {
        // 添加一个元素之前，需要先获得一个许可
        semaphore.acquire();
        boolean wasAdded = false;
        try {
            wasAdded = set.add(obj);
            return wasAdded;
        } finally {
            // 如果add操作没有添加任何元素，那么会立刻释放许可
            if (! wasAdded) {
                semaphore.release();
            }
        }
    }

    /**
     * 删除操作
     *
     * @param obj 待删除对象
     * @return 是否删除成功
     */
    public boolean remove(T obj) {
        boolean wasRemoved = set.remove(obj);
        if (wasRemoved) {
            // 若删除一个元素，就会释放一个许可
            semaphore.release();
        }
        return wasRemoved;
    }
}
