package com.cnblogs.reminis;

/**
 * @author Mr.Sun
 * @date 2022年09月09日 20:56
 *
 * 对象发布
 */
public class UnsafeStates {
    private String[] states = new String[]{
            "AK", "AL" /*...*/
    };

    public String[] getStates() {
        return states;
    }
}
