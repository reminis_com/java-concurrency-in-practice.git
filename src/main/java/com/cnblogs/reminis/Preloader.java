﻿package com.cnblogs.reminis;

import net.jcip.examples.LaunderThrowable;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author: Mr.Sun
 * @create: 2022-11-14 09:54
 * @description: 使用FutureTask来提前加载稍后需要的数据
 **/
public class Preloader {
    private final FutureTask<ProductInfo> future = new FutureTask<>(new Callable<ProductInfo>() {
        @Override
        public ProductInfo call() throws DataLoadException {
            // 模拟 从数据库加载产品信息
            return loadProductInfo();
        }
    });

    private final Thread thread = new Thread(future);

    /**
     * 由于在构造函数或静态初始化方法中启动线程并不是一种好方法，因此提供了一个start()方法来启动线程
     */
    public void start() { thread.start(); }

    /**
     * 获取产品信息
     *
     * @return 加载好的产品信息
     */
    public ProductInfo get() throws DataLoadException, InterruptedException {
        try {
            return future.get();
        } catch (ExecutionException e) {
            Throwable cause = e.getCause();
            if (cause instanceof DataLoadException)
                throw (DataLoadException) cause;
            else
                throw LaunderThrowable.launderThrowable(cause);
        }
    }

    /**
     * 高开销计算实现逻辑
     *
     * @throws DataLoadException 数据加载异常
     */
    private ProductInfo loadProductInfo() throws DataLoadException {

        return null;
    }
}

class ProductInfo {}

class DataLoadException extends Exception { }