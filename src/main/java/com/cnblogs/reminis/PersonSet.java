package com.cnblogs.reminis;

import net.jcip.annotations.ThreadSafe;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Mr.Sun
 * @date 2022年09月10日 16:49
 *
 * 通过封闭机制实现线程安全
 */
@ThreadSafe
public class PersonSet {
    private final Set<Person> mySet = new HashSet<Person>();

    public synchronized void addPerson(Person p) {
        mySet.add(p);
    }

    public synchronized boolean containsPerson(Person p) {
        return mySet.contains(p);
    }

    interface Person {
    }
}
