package net.jcip.examples;

import java.util.concurrent.CountDownLatch;

/**
 * TestHarness
 * <p/>
 * Using CountDownLatch for starting and stopping threads in timing tests
 *
 * ---
 * ��ʾCountDownLatch
 *
 * @author Brian Goetz and Tim Peierls
 */
public class TestHarness {

    public static void main(String[] args) throws InterruptedException {
        //ִ�����񣬲��ҷ��غ�ʱ
        long time = timeTasks(3, new Runnable() {
            @Override
            public void run() {
                System.out.println("gzh");
            }
        });

        //��ӡ����ҵ���̵߳ĺ�ʱ
        System.out.println(time);
    }

    /**
     *
     * @author gzh
     * @date 2022/1/19 10:33 AM
     * @param nThreads
     * @param task
     * @return long ����������ܺ�ʱ(����)
     */
    public static long timeTasks(int nThreads, final Runnable task)
            throws InterruptedException {
        // ����������
        final CountDownLatch startGate = new CountDownLatch(1); //��ʼ��
        final CountDownLatch endGate = new CountDownLatch(nThreads); //������

        // ִ�������߳�
        for (int i = 0; i < nThreads; i++) {
            Thread t = new Thread() {
                public void run() {
                    try {
                        startGate.await(); //���������߳̾���Ҫ�ȴ����߳�׼��������ɣ����ܿ�ʼִ��
                        try {
                            task.run(); //ִ������
                        } finally {
                            endGate.countDown(); //ÿ��ҵ���߳�ִ�����֮�󣬽����ż�1
                        }
                    } catch (InterruptedException ignored) {
                    }
                }
            };
            t.start();
        }

        //���߳�׼������
        long start = System.nanoTime();
        System.out.println("���߳�׼������������");
        startGate.countDown(); //ֻ�����߳�׼��������ɣ�������ʼִ��ҵ���߳�

        //������ҵ���߳�ȫ��ִ�����֮�󣬽����ŲŻ����
        endGate.await();
        long end = System.nanoTime();
        return end - start;
    }
}
